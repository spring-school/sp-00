package ru.volnenko.se.dto;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

public class Domain {

    private List<Project> projects;
    private List<Task> tasks;

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
