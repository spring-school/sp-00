package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandAbsentException;
import ru.volnenko.se.error.CommandCorruptException;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Denis Volnenko
 */

@Component
public final class Bootstrap {

    @Autowired
    public ITerminalService terminalService;

    @Autowired
    private List<AbstractCommand> abstractCommands;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() throws Exception {
        if (abstractCommands == null || abstractCommands.size() == 0) throw new CommandAbsentException();
        for (AbstractCommand command : abstractCommands) {
            registry(command);
        }

        start();
    }

    public void registry(final AbstractCommand command) {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}
