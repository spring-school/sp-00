package ru.volnenko.se.command.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;

import java.util.List;

/**
 * @author Denis Volnenko
 */

@Component
public final class HelpCommand extends AbstractCommand {

    @Autowired
    private List<AbstractCommand> abstractCommands;

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : abstractCommands) {
            System.out.println(command.command() + ": " + command.description());
        }
    }

}
