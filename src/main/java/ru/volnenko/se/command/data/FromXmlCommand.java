package ru.volnenko.se.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.dto.Domain;

import java.io.IOException;
import java.io.InputStream;

@Component
public final class FromXmlCommand extends AbstractCommand {

    @Autowired
    private IDomainService domainService;

    @Override
    public String command() {
        return "from-xml";
    }

    @Override
    public String description() {
        return "Load data from XML";
    }

    @Override
    public void execute() throws IOException {

        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("domain.xml");
        final ObjectMapper mapper = new XmlMapper();
        final Domain domain = mapper.readValue(inputStream, Domain.class);

        domainService.setDomain(domain);
        System.out.println("[DATA LOADED]");
    }
}