package ru.volnenko.se.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.dto.Domain;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
public final class ToJsonCommand extends AbstractCommand {

    @Autowired
    private IDomainService domainService;

    @Override
    public String command() {
        return "to-json";
    }

    @Override
    public String description() {
        return "Transfer data to JSON";
    }

    @Override
    public void execute() throws IOException {

        final Domain domain = domainService.getDomain();
        if (domain == null) return;

        final ObjectMapper mapper = new ObjectMapper();
        final String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final File jsonOutput = new File("C:\\_school\\new.json");
        final FileWriter fileWriter = new FileWriter(jsonOutput);

        fileWriter.write(prettyJson);
        fileWriter.close();

        System.out.println("[DATA SAVED]");
    }
}
