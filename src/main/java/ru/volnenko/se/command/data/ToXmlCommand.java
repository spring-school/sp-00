package ru.volnenko.se.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.dto.Domain;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class ToXmlCommand extends AbstractCommand {

    @Autowired
    private IDomainService domainService;

    @Override
    public String command() {
        return "to-xml";
    }

    @Override
    public String description() {
        return "Transfer data to XML";
    }

    @Override
    public void execute() throws IOException {

        final Domain domain = domainService.getDomain();
        if (domain == null) return;

        final ObjectMapper mapper = new XmlMapper();
        final String prettyXml = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);


        System.out.println(prettyXml);
        final File jsonOutput = new File("C:\\_school\\new2.xml");
        final FileWriter fileWriter = new FileWriter(jsonOutput);

        fileWriter.write(prettyXml);
        fileWriter.close();

        System.out.println("[DATA SAVED]");
    }
}
