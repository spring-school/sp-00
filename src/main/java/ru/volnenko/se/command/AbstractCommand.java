package ru.volnenko.se.command;

import org.springframework.stereotype.Component;

/**
 * @author Denis Volnenko
 */

@Component
public abstract class AbstractCommand {

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

}
