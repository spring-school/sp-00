package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.dto.Domain;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

@Service
public class DomainService implements IDomainService {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public Domain getDomain() {
        List<Project> projects = projectRepository.getListProject();
        List<Task> tasks = taskRepository.getListTask();
        Domain domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        return domain;
    }

    @Override
    public void setDomain(final Domain domain) {
        if (domain.getProjects() != null && !domain.getProjects().isEmpty()) {
            final List<Project> projects = domain.getProjects();
            projectRepository.load(projects);
        }

        if (domain.getTasks() != null && !domain.getTasks().isEmpty()) {
            final List<Task> tasks = domain.getTasks();
            taskRepository.load(tasks);
        }
    }
}
