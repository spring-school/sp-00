package ru.volnenko.se.api.service;

/**
 * @author Denis Volnenko
 */
public interface ITerminalService {

    String nextLine();

    Integer nextInteger();

}
